#!/bin/bash

set +x

repo=$1
branch=$2
mainFolder=$3

rm -rf diff.txt
cd $repo

git checkout -b $branch origin/$branch

git diff --name-only main | grep "/pli/" > diff.txt

version=A01

find $mainFolder/link/ -name P* >> link.txt
finalMessageResults=0
while read line; 
do 
   fiveCharModWithExt=${line##*/}
   fiveCharMod=$(echo $fiveCharModWithExt | cut -c1-5 )
   moduleinAtleastOneProg=0
   while read linkLine; 
   do
      if [ "$(grep -c $fiveCharMod $linkLine)" -ge 1 ]; then
         ((moduleinAtleastOneProg=moduleinAtleastOneProg+1))
         if [ "$(grep -c $fiveCharMod$version $linkLine)" -eq 0 ]; then
            echo "$fiveCharMod$version is not in ${linkLine##*/}" >> results.txt
         fi
      fi
   done < link.txt

   if [ $moduleinAtleastOneProg -eq 0 ]; then
      echo "$fiveCharMod$version is not in any linkdecks" >> results.txt
   fi

done < diff.txt

